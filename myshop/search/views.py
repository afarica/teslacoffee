from django.views.generic import  ListView
from shop.models import Product
from django.db.models import Q
class SearchView(ListView):
    model = Product
    template_name = 'search.html'
 
    def get_queryset(self):
        query = self.request.GET.get('q')
        object_list = Product.objects.filter(
            Q(name__icontains=query) | Q(image__icontains=query)
        )
        return object_list